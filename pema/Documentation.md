![](./Images/108.png)


# GitLab 

![](./Images/git.png)

## What is GitLab?

GitLab is a platform where people can work together on software projects. It's like a virtual space where developers can store their code, collaborate with others, and keep track of changes they make to the code. 

Think of it like a shared folder for a group project. Everyone involved can see the files, make changes, and leave comments. GitLab helps manage this process by providing tools for version control, issue tracking, continuous integration, and more.

In simple terms, GitLab is a place where developers can work together efficiently, making sure everyone is on the same page and that the project progresses smoothly.

### 1. Go to GitLab and create an account
![](./Images/step1.png)
![](./Images/step2.png)
![](./Images/step3.png)
![](./Images/step5.png)
![](./Images/step8.png)
Now you are done with creating your account

### 2. Download gitbash
#### How to download gitbash on windows
![](./Images/down1.png)
![](./Images/down2.png)
![](./Images/down3.png)
![](./Images/down4.png)
![](./Images/down5.png)
![](./Images/down6.png)
![](./Images/down7.png)
![](./Images/down8.png)
Now you are done with downloading gitbash on your windows.

### 3. Let's Git Command!!
![](./Images/gitvers.png)

You will find your Git Version that is in your laptop.

![](./Images/Gitvers2.png)

#### Lets run some git commands.
Get your user name in your gitlab in your laptop.
![](./Images/gituser.png)
 
Then git configure your user email.
![](./Images/gitemail.png)
![](./Images/gituser2.png)
## What Is a fork?
In GitLab, a "fork" is like making a copy of a project. When you fork a project, you create your own separate version of it. This allows you to make changes to the project without affecting the original version. You can work on your forked version independently, making modifications, fixes, or adding new features. Later on, you can propose these changes back to the original project by creating a "merge request" or a "pull request," so the original project owners can review and potentially include your changes. Forks are commonly used in collaborative projects where multiple people may want to contribute changes without directly altering the original project.

### How to fork a project
1. Login to your gilab and go to your project
![](./Images/fork1.png)
![](./Images/fork2.png)
![](./Images/fork3.png)
![](./Images/fork4.png)
![](./Images/fork5.png)

## How to download Flameshot for doing screen shots and documentations
1. Go to google chrome, search for flame shot and click on the flameshot website
![](./Images/flame1.png)
2. Click on the download button
![](./Images/flame2.png)
3. Click on the download installer button
![](./Images/flame.3png)
4. Go too chrome://downloads and click on your recently downloaded flameshot
![](./Images/flame4.png)
5. Click next and finish it
![](./Images/flame5.png)
6. Then go to your desktop and click on your flameshot app
![](./Images/flame6.png)
![](./Images/flame7.png)
![](./Images/flame8.png)















